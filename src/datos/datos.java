/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import GUI.frmMenu;
import GUI.frmUpdate;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabri
 */
public class datos {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;

    public void Conexion() {
        if (connection != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/proyecto";
        String password = "Sheldon1995";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            System.out.println("Problem when connecting to the database:" + e);
        }
    }

    public void insertarMascota(String id, String nombre, String sexo, String edad, String tamaño, String color, String tipo, String ubicacion, FileInputStream fis, int longi) {
        try {
            this.Conexion();
            String estado = "Disponible";
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime fecha_ingreso = LocalDateTime.now();
            String sql = "INSERT INTO mascotas (id, nombre, sexo, edad, tamaño, color, tipo, foto , ubicacion, fecha_ingreso, estado)"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps;
            ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, nombre);
            ps.setString(3, sexo);
            ps.setString(4, edad);
            ps.setString(5, tamaño);
            ps.setString(6, color);
            ps.setString(7, tipo);
            ps.setBinaryStream(8, fis, longi);
            ps.setString(9, ubicacion);
            ps.setString(10, dtf.format(fecha_ingreso));
            ps.setString(11, estado);
            ps.execute();
            ps.close();

        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public void actualizarMascota(String id, String nombre, String sexo, String edad, String tamaño, String color, String tipo, String ubicacion, FileInputStream fis, int longi) {
        try {
            this.Conexion();
            // String estado = "Disponible";
            //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            // LocalDateTime fecha_ingreso = LocalDateTime.now();
            String sql = "UPDATE  mascotas set nombre = ?, sexo = ?, edad = ?, tamaño = ?, color = ?, tipo = ?, foto = ? , ubicacion = ? where id='" + id + "'";
            PreparedStatement ps;
            ps = connection.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, sexo);
            ps.setString(3, edad);
            ps.setString(4, tamaño);
            ps.setString(5, color);
            ps.setString(6, tipo);
            ps.setBinaryStream(7, fis, longi);
            ps.setString(8, ubicacion);
            ps.execute();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public void actualizarMascotaSinFoto(String id, String nombre, String sexo, String edad, String tamaño, String color, String tipo, String ubicacion) {
        try {
            this.Conexion();
            // String estado = "Disponible";
            //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            // LocalDateTime fecha_ingreso = LocalDateTime.now();
            String sql = "UPDATE  mascotas set nombre = ?, sexo = ?, edad = ?, tamaño = ?, color = ?, tipo = ?,  ubicacion = ? where id='" + id + "'";
            PreparedStatement ps;
            ps = connection.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, sexo);
            ps.setString(3, edad);
            ps.setString(4, tamaño);
            ps.setString(5, color);
            ps.setString(6, tipo);
            ps.setString(7, ubicacion);
            ps.execute();
            ps.close();
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public void eliminarMascota(String id) {
        try {
            this.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("delete from mascotas where id= '" + id + "'");
            if (z == 1) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Error al eliminar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public String[][] leerMascotas() {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas ");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + "Foto" + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }

    public String BusquedaMascotas(String id) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime fecha_ingreso = LocalDateTime.now();
        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas where id='" + id + "' ");

            while (rs.next()) {
                resultado = "Nombre: " + rs.getString("nombre") + "  Sexo: " + rs.getString("sexo")
                        + "   Edad:  " + rs.getString("edad") + "  Tamaño: " + rs.getString("tamaño") + "   Color:  " + rs.getString("color")
                        + "   Tipo: " + rs.getString("tipo") + "  Fecha de Ingreso:  " + rs.getString("fecha_ingreso") + "  Fecha de adopción:  " + dtf.format(fecha_ingreso);
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }

        return resultado;

    }

    public InputStream leerMascotasFoto(String id) {
        InputStream datos = null;
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas where id='" + id + "'");

            while (rs.next()) {
                datos = rs.getBinaryStream(8);
            }

        } catch (SQLException e) {
            System.out.println("Error de conexión" + e);
        }
        System.err.println(datos);
        return datos;

    }

    public String[][] returnMatriz10(String reporte) {
        String[] linea = reporte.split(";");
        String[][] matriz = new String[linea.length][11];
        for (int i = 0; i < linea.length; i++) {
            String[] datos = linea[i].split(",");
            matriz[i][0] = datos[0];
            matriz[i][1] = datos[1];
            matriz[i][2] = datos[2];
            matriz[i][3] = datos[3];
            matriz[i][4] = datos[4];
            matriz[i][5] = datos[5];
            matriz[i][6] = datos[6];
            matriz[i][7] = datos[7];
            matriz[i][8] = datos[8];
            matriz[i][9] = datos[9];
            matriz[i][10] = datos[10];

        }
        return matriz;
    }

    public String[][] returnMatriz4(String reporte) {
        String[] linea = reporte.split(";");
        String[][] matriz = new String[linea.length][5];
        for (int i = 0; i < linea.length; i++) {
            String[] datos = linea[i].split(",");
            matriz[i][0] = datos[0];
            matriz[i][1] = datos[1];
            matriz[i][2] = datos[2];
            matriz[i][3] = datos[3];
            matriz[i][4] = datos[4];

        }
        return matriz;
    }

    public String[][] returnMatriz3(String reporte) {
        String[] linea = reporte.split(";");
        String[][] matriz = new String[linea.length][4];
        for (int i = 0; i < linea.length; i++) {
            String[] datos = linea[i].split(",");
            matriz[i][0] = datos[0];
            matriz[i][1] = datos[1];
            matriz[i][2] = datos[2];
            matriz[i][3] = datos[3];

        }
        return matriz;
    }

    public String[][] leerUsuarios() {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from usuarios");

            while (rs.next()) {
                resultado += rs.getString("cedula") + "," + rs.getString("nombre") + "," + rs.getString("correo")
                        + "," + rs.getString("password") + "," + rs.getString("tipo") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz4(resultado);
        return datos;

    }
    public String[] leerClientes() {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select cedula from usuarios where tipo = 'cliente'");

            while (rs.next()) {
                resultado += rs.getString("cedula") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[] datos = resultado.split(";");
        return datos;

    }

    public String buscarCorreoSoli(String id) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select correo from usuarios where cedula ='" + id + "'");

            while (rs.next()) {
                resultado += rs.getString(1);
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }

        return resultado;

    }

    public String[][] leerMascotasDisponibles() {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }

    public void cambiarEstadoMascota(String id) {
        try {
            this.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE mascotas set estado = 'bloqueado' WHERE id = '" + id + "'");
            if (z == 1) {
                System.out.println("Se modifico el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }
    public void cambiarEstadoMascotaDisponible(String id) {
        try {
            this.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE mascotas set estado = 'Disponible' WHERE id = '" + id + "'");
            if (z == 1) {
                System.out.println("Se modifico el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }
    public void cambiarEstadoMascotaAdoptado(String id) {
        try {
            this.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE mascotas set estado = 'Adoptado' WHERE id = '" + id + "'");
            if (z == 1) {
                System.out.println("Se modifico el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }
    

    public void crearSolicitud(String mascota, String usuario) {
        try {
            this.Conexion();
            String estado = "Pendiente";
            String id = mascota + usuario;
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO solicitudes (id, cedula, id_mascota, estado) "
                    + "VALUES ('" + id + "', '" + usuario + "', '" + mascota + "', '"
                    + estado + "')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public String[][] leerSolicitudes() {
        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from solicitudes");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("cedula") + "," + rs.getString("id_mascota")
                        + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz3(resultado);
        return datos;
    }

    public void cambiarEstadoSolicitud(String id, String respuesta) {
        try {
            this.Conexion();
            if (respuesta.equalsIgnoreCase("aceptar")) {
                s = connection.createStatement();
                int z = s.executeUpdate("UPDATE solicitudes set estado = 'Aprobada' WHERE id = '" + id + "'");
                if (z == 1) {
                    System.out.println("Se modifico el registro de manera exitosa");
                } else {
                    System.out.println("Error al modificar el registro");
                }
            } else if (respuesta.equalsIgnoreCase("rechazar")) {
                s = connection.createStatement();
                int z = s.executeUpdate("UPDATE solicitudes set estado = 'Rechazada' WHERE id = '" + id + "'");
                if (z == 1) {
                    System.out.println("Se modifico el registro de manera exitosa");
                } else {
                    System.out.println("Error al modificar el registro");
                }
            }

        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }

    public void enviarCorrreoRechazo(String mailAdmin, String password, String mailClient, String mensaje) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailAdmin, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailAdmin));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mailClient));
            message.setSubject("Solicitud de adopción");
            message.setText(mensaje);

            Transport.send(message);
            JOptionPane.showMessageDialog(null, "Su mensaje ha sido enviado");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public void enviarCorrreoAceptado(String mailAdmin, String password, String mailClient, String pMensaje, String idAnimal) {
        try {
            InputStream is = leerMascotasFoto(idAnimal);
            ImageIcon foto;
            BufferedImage bi;
            try {
                bi = ImageIO.read(is);
                foto = new ImageIcon(bi);
                Image img = foto.getImage();
                ImageIO.write((RenderedImage) img, "jpg", new File("foto.jpg"));
            } catch (IOException ex) {
                System.err.println("errores" + ex);
                Logger.getLogger(frmMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
            String datos = BusquedaMascotas(idAnimal);
            // se obtiene el objeto Session. La configuración es para
            // una cuenta de gmail.
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);

            // Se compone la parte del texto
            BodyPart texto = new MimeBodyPart();
            texto.setText(pMensaje + "\nDetalles de la solicitud: \n" + datos);

            // Se compone el adjunto con la imagen
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(
                    new DataHandler(new FileDataSource("foto.jpg")));
            adjunto.setFileName("foto.jpg");

            // Una MultiParte para agrupar texto e imagen.
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailAdmin));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(mailClient));
            message.setSubject("Solicitud de adopción");
            message.setContent(multiParte);

            // Se envia el correo.
            Transport t = session.getTransport("smtp");
            t.connect(mailAdmin, password);
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            JOptionPane.showMessageDialog(null, "Su mensaje ha sido enviado");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void registrarUsuario(String id, String nombre, String correo, String contraseña) {
        try {
            this.Conexion();
            String tipo = "cliente";
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO usuarios (cedula, nombre, correo, password, tipo) "
                    + "VALUES ('" + id + "', '" + nombre + "', '" + correo + "', md5('"
                    + contraseña + "'), '" + tipo+ "')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }
    }
    public String[][] filtroTipo(String tipo) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible' and tipo = '" + tipo + "'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }
    public String[][] filtroUbicacion(String ubicacion) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible' and ubicacion = '" + ubicacion + "'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }
    public String[][] filtroEdad(int desde, int hasta) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible' and cast(edad AS INTEGER) > " + desde + " and cast(edad AS INTEGER) < " + hasta);

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }
    public String[][] leerMascotasDisponiblesTipo(String tipo) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible' and tipo ='" + tipo +"'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }
    public String[][] leerMascotasDisponiblesUbicacion(String ubicacion) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from mascotas WHERE estado = 'Disponible' and ubicacion = '" + ubicacion + "'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("nombre") + "," + rs.getString("sexo")
                        + "," + rs.getString("edad") + "," + rs.getString("tamaño") + "," + rs.getString("color")
                        + "," + rs.getString("tipo") + "," + rs.getString("foto") + "," + rs.getString("ubicacion")
                        + "," + rs.getString("fecha_ingreso") + "," + rs.getString("estado") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz10(resultado);
        return datos;

    }
    public String[][] leerHistorialUsuario(String usuario) {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select id, id_solicitud, id_mascota, fecha_adopcion from historial where cedula = '" + usuario + "'");

            while (rs.next()) {
                resultado += rs.getString("id") + "," + rs.getString("id_solicitud") + "," + rs.getString("id_mascota")
                        + "," + rs.getString("fecha_adopcion") + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz3(resultado);
        return datos;

    }
    public void insetarDatosHistorial(String idSoli, String cedula, String idMascota) {
        try {
            this.Conexion();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime fecha_ingreso = LocalDateTime.now();
            String sql = "INSERT INTO historial (id_solicitud, cedula, id_mascota, fecha_adopcion)"
                    + " VALUES (?,?,?,?)";
            PreparedStatement ps;
            ps = connection.prepareStatement(sql);
            ps.setString(1, idSoli);
            ps.setString(2, cedula);
            ps.setString(3, idMascota);
            ps.setString(4, dtf.format(fecha_ingreso));
            ps.execute();
            ps.close();

        } catch (Exception e) {
            System.out.println("Error de conexión: " + e);
        }

    }

    public String[][] leerHistorial() {

        String resultado = "";
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select * from historial ");

            while (rs.next()) {
                resultado += rs.getInt(1) + "," + rs.getString(2) + "," + rs.getString(3)
                        + "," + rs.getString(4) + "," + rs.getString(5) + ";";
            }

        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        String[][] datos = this.returnMatriz4(resultado);
        return datos;

    }

    public int[] graficoPorFecha(Date desde, Date hasta) {
        int[] retorno = new int[2];
        SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
        System.err.println(d.format(hasta));
        int x = 0;
        try {
            this.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select  count(tipo) from mascotas ms, historial hs where ms.id"
                    + " = hs.id_mascota and cast (hs.fecha_adopcion as date) > '" + d.format(desde) + "'  and cast (hs.fecha_adopcion as date)< '" + d.format(hasta) + "' group by tipo");
            while (rs.next()) {
                retorno[x] = rs.getInt(1);
                x++;
            }
        } catch (Exception e) {
            System.out.println("Error de conexión" + e);
        }
        //System.err.println(retorno[0]+" adass"+retorno[1]);
        return retorno;
    }
}
