/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.datos;
import java.security.MessageDigest;

/**
 *
 * @author fabri
 */
public class Negocio {
    datos oDatos = new datos();
    
    public String login(String usuario, String contraseña) {
        String tipo="";
        try {
            
            String[][] usuarios = oDatos.leerUsuarios();
            String passwordMD5 = null;
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] tmp = contraseña.getBytes();
            md5.update(tmp);
            passwordMD5 = byteArrToString(md5.digest());
            System.out.println(passwordMD5);
            for (int i = 0; i < usuarios.length; i++) {
                if (usuario.equals(usuarios[i][0]) && passwordMD5.equalsIgnoreCase(usuarios[i][3])) {
                    tipo = usuarios[i][4];
                    break;
                } else {
                    tipo = "Usuario o contraseña incorrecto";
                }
            }
        } catch (Exception e) {
            System.out.println("error: " + e);
        }

        return tipo;
    }

    private static String byteArrToString(byte[] b) {
        String res = null;
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            int j = b[i] & 0xff;
            if (j < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(j));
        }
        res = sb.toString();
        return res.toUpperCase();
    }
    
}
